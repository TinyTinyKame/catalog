'use strict'

/**
 * Catalogs model
 */
const Model = require('objection').Model;
const path  = require('path');

class Catalogs extends Model {
    static get tableName() {
        return 'catalogs';
    }

    static get idColumn() {
        return 'id';
    }

    static get jsonSchema() {
        return {
            type:       'object',
            required:   ['name', 'code'],
            properties: {
                id:              { type: 'integer' },
                name:            { type: 'string', minLength: 1, maxLength: 255 },
                code:            { type: 'string', minLength: 1, maxLength: 255 },
                number_products: { type: 'integer', default: 0 }
            }
        };
    }

    static get relationMappings() {
        return {
            products: {
                relation: Model.HasManyRelation,
                modelClass: path.resolve(__dirname, 'Products.model'),
                join: {
                    from: 'catalogs.id',
                    to:   'products.catalog_id'
                }
            }
        };
    }
}

module.exports = Catalogs;
