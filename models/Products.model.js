'use strict'

/**
 * Products model
 */
const Model    = require('objection').Model;
const path     = require('path');
const Catalogs = require('./Catalogs.model');

class Products extends Model {
    static get tableName() {
        return 'products';
    }

    static get idColumn() {
        return 'id';
    }

    static get jsonSchema() {
        return {
            type:       'object',
            required:   ['name', 'code', 'description', 'price', 'weight', 'catalog_id'],
            properties: {
                id:          { type: 'integer' },
                name:        { type: 'string', minLength: 1, maxLength: 255 },
                code:        { type: 'string', minLength: 1, maxLength: 255 },
                description: { type: 'string' },
                price:       { type: 'number' },
                weight:      { type: 'number' },
                catalog_id:  { type: 'integer' }
            }
        };
    }

    static get relationMappings() {
        return {
            catalog: {
                relation: Model.BelongsToOneRelation,
                modelClass: path.resolve(__dirname, 'Catalogs.model'),
                join: {
                    from: 'products.catalog_id',
                    to:   'catalogs.id'
                }
            }
        };
    }

    // $afterInsert(queryContext) {
    //     console.log(queryContext);
    // }
}

module.exports = Products;
