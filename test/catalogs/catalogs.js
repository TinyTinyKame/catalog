const supertest = require("supertest");
const app       = require("../../app.js");
const api       = supertest(app);

describe('*** CATALOGS UNIT_TEST ***', () => {
    it('I should receive all catalogs', (done) => {
        api.get('/catalogs')
        .send({})
        .expect(200, [
            {
                id: 1,
                name: "Catalog 1",
                code: "qwerty123456",
                number_products: 2,
                products: [
                    {
                        id: 1,
                        name: "Product 1",
                        code: "zxcvbn123456",
                        description: "Product 1 description",
                        price: 5.99,
                        weight: 1.4,
                        catalog_id: 1
                    },
                    {
                        id: 2,
                        name: "Product 2",
                        code: "plokmi789012",
                        description: "Product 2 description",
                        price: 10.99,
                        weight: 3.4,
                        catalog_id: 1
                    }
                ]
            },
            {
                id: 2,
                name: "Catalog 2",
                code: "asdfgh789012",
                number_products: 0,
                products: []
            }
        ])
        .end(done);
    });

    it('I should receive 1 catalog', (done) => {
        api.get('/catalogs?limit=1')
        .send({})
        .expect(200, [
            {
                id: 1,
                name: "Catalog 1",
                code: "qwerty123456",
                number_products: 2,
                products: [
                    {
                        id: 1,
                        name: "Product 1",
                        code: "zxcvbn123456",
                        description: "Product 1 description",
                        price: 5.99,
                        weight: 1.4,
                        catalog_id: 1
                    },
                    {
                        id: 2,
                        name: "Product 2",
                        code: "plokmi789012",
                        description: "Product 2 description",
                        price: 10.99,
                        weight: 3.4,
                        catalog_id: 1
                    }
                ]
            }
        ])
        .end(done);
    });

    it('I should receive all catalogs from page 2', (done) => {
        api.get('/catalogs?page=2')
        .send({})
        .expect(200, [])
        .end(done);
    });

    it('I should receive all products from catalog 1', (done) => {
        api.get('/catalogs/1/products')
        .send({})
        .expect(200, {
            id: 1,
            name: "Catalog 1",
            code: "qwerty123456",
            number_products: 2,
            products: [
                {
                    id: 1,
                    name: "Product 1",
                    code: "zxcvbn123456",
                    description: "Product 1 description",
                    price: 5.99,
                    weight: 1.4,
                    catalog_id: 1
                },
                {
                    id: 2,
                    name: "Product 2",
                    code: "plokmi789012",
                    description: "Product 2 description",
                    price: 10.99,
                    weight: 3.4,
                    catalog_id: 1
                }
            ]
        })
        .end(done);
    });

    it('I should receive an error 404', (done) => {
        api.get('/catalogs/1337/products')
        .send({})
        .expect(404, '"Catalog 1337 not found"')
        .end(done);
    });
});
