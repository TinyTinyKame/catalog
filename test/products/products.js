const supertest = require("supertest");
const app       = require("../../app.js");
const api       = supertest(app);

describe('*** PRODUCTS UNIT_TEST ***', () => {
    it('I should receive the inserted product after the insert', (done) => {
        api.post('/catalogs/1/products')
        .send({
            name: 'Product 3',
            code: 'jiuhbn123456',
            description: 'Product 3 description',
            price: 3.99,
            weight: 5
        })
        .expect(201, {})
        .end(done);
    });
});
