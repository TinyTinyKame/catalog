'use strict'

const app  = require("../app.js");
const knex = app.get('knex');

beforeEach((done) => {
    knex.migrate.latest().then(() => {
        knex.seed.run().then(() => {
            done();
        });
    });
});
