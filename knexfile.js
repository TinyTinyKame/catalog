// Update with your config settings.
// Il faut changer les trucs pour que ça soit l'env qui gère tout :)
module.exports = {
    testing: {
        client: 'pg',
        connection: {
            database: 'test_catalogs',
            user:     'usertest_catalogs',
            password: '',
            host:     '127.0.0.1'
        },
        migrations: {
            tableName: 'knex_migrations'
        },
        seeds: {
            directory: './seeds/testing'
        }
    }
};
