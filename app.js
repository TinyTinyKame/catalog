'use strict'

const express    = require('express');
const validator  = require('express-validator');
const logger     = require('morgan');
const bodyParser = require('body-parser');
const cors       = require('cors');
const knex       = require('knex');
const knexfile   = require('./knexfile.js');
const Model      = require('objection').Model;

// Database
const knex_config = knex(knexfile[process.env.NODE_ENV]);
Model.knex(knex_config);
const app = express();
app.set('knex', knex_config);

// Active les CORS
app.use(cors());

// Logger using morgan
app.use(logger('combined'));

// Parse JSON and only JSON in body
app.use(bodyParser.json());

// Adding express-validator
app.use(validator({
    customValidators: {
        gte: (param, num) => {
            return param >= num;
        },
        isTypeOf: (param, type) => {
            return typeof param === type;
        }
    }
}));

// Routes
app.use("/", require("./routes/catalogs.routes"));
app.use("/", require("./routes/products.routes"));

// Error handler
app.use((err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }
    res.status(err.status);
    res.json(err.message);
});

console.log('Catalogs-api launched');

module.exports = app;
