# catalog-api

## Database

- `createdb test_catalogs`
- `createuser usertest_catalogs --interactive`

## How to launch

 - `yarn install`
 - `yarn run local_migration`
 - `yarn run testing`

# Tests

 - `yarn run unit_test`
