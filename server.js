const app  = require('./app');
const port = process.env.CATALOG_PORT || 8080;

// Starting
app.listen(parseInt(port));
