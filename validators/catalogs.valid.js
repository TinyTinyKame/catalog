'use strict'

/**
 * Validators for catalogs
 *
 * Every functions are used to validate field types
 */
const validator = require('express-validator');

module.exports.checkCatalogId = async (req, res, next) => {
    req.checkParams('catalog_id', 'catalog_id should be a positive number').isInt().gte(1);

    // Validity check
    let validate;
    try {
        validate = await req.getValidationResult();
    } catch (err) {
        console.log(err);

        return res.status(400).json('Error validating catalog_id');
    }

    if (!validate.isEmpty()) {
        return res.status(400).json(validate.array());
    }

    next();
};
