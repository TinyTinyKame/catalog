'use strict'

/**
 * Validators for products
 *
 * Every functions are used to validate field types
 */
const validator = require('express-validator');

module.exports.checkProductId = async (req, res, next) => {
    req.checkParams('product_id', 'product_id should be a positive number').isInt().gte(1);

    // Validity check
    let validate;
    try {
        validate = await req.getValidationResult();
    } catch (err) {
        console.log(err);

        return res.status(400).json('Error validating product_id');
    }

    if (!validate.isEmpty()) {
        return res.status(400).json(validate.array());
    }

    next();
};

module.exports.checkProductParams = async (req, res, next) => {
    req.checkBody('name', 'A name must be provided').notEmpty();
    req.checkBody('name', 'name must be a string').isTypeOf('string');
    req.checkBody('code', 'A code must be provided').notEmpty();
    req.checkBody('code', 'code must be a string').isTypeOf('string');
    req.checkBody('description', 'A description must be provided').notEmpty();
    req.checkBody('description', 'description must be a string').isTypeOf('string');
    req.checkBody('price', 'A price must be provided').notEmpty();
    req.checkBody('price', 'price must be a float').isTypeOf('number');
    req.checkBody('weight', 'A weight must be provided').notEmpty();
    req.checkBody('weight', 'weight must be a float').isTypeOf('number');

    // Validity check
    let validate;
    try {
        validate = await req.getValidationResult();
    } catch (err) {
        console.log(err);

        return res.status(400).json('Error validating product_id');
    }

    if (!validate.isEmpty()) {
        return res.status(400).json(validate.array());
    }

    next();
}
