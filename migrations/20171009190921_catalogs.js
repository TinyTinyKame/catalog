// Migration for database

module.exports.up = (knex, Promise) => {
    return knex.schema.createTableIfNotExists('catalogs', (table) => {
        table.increments('id');
        table.string('name').notNullable();
        table.string('code').notNullable();
        table.integer('number_products').defaultTo(0);
    }).then(() => {
        return knex.schema.createTableIfNotExists('products', (table) => {
            table.increments('id');
            table.string('name').notNullable();
            table.string('code').notNullable();
            table.text('description').notNullable();
            table.float('price').notNullable();
            table.float('weight').notNullable();
            table.integer('catalog_id').unsigned().notNullable();
        });
    });
};

module.exports.down = (knex, Promise) => {
    return Promise.all([
        knex.schema.dropTableIfExists('products')
    ]).then(() => {
        return Promise.all([
            knex.schema.dropTableIfExists('catalogs')
        ]);
    });
};
