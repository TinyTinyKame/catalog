// Tables and datas for test

const populateData = (knex, Promise) => {
    return Promise.all([
        knex('catalogs').insert({
            id:              1,
            name:            'Catalog 1',
            code:            'qwerty123456',
            number_products: 2
        }),
        knex('catalogs').insert({
            id:              2,
            name:            'Catalog 2',
            code:            'asdfgh789012',
            number_products: 0
        })
    ]).then(() => {
        return Promise.all([
            knex('products').insert({
                id:          1,
                name:        'Product 1',
                code:        'zxcvbn123456',
                description: 'Product 1 description',
                price:       5.99,
                weight:      1.4,
                catalog_id:  1
            }),
            knex('products').insert({
                id:          2,
                name:        'Product 2',
                code:        'plokmi789012',
                description: 'Product 2 description',
                price:       10.99,
                weight:      3.4,
                catalog_id:  1
            })
        ]);
    });
}

const createTables = (knex, Promise) => {
    return knex.schema.createTableIfNotExists('catalogs', (table) => {
        table.increments('id');
        table.string('name').notNullable();
        table.string('code').notNullable();
        table.integer('number_products').defaultTo(0);
    }).then(() => {
        return knex.schema.createTableIfNotExists('products', (table) => {
            table.increments('id');
            table.string('name').notNullable();
            table.string('code').notNullable();
            table.text('description').notNullable();
            table.float('price').notNullable();
            table.float('weight').notNullable();
            table.integer('catalog_id').unsigned().notNullable();
        });
    });
}

const dropTables = (knex, Promise) => {
    return Promise.all([
        knex.schema.dropTableIfExists('products')
    ]).then(() => {
        return Promise.all([
            knex.schema.dropTableIfExists('catalogs')
        ]);
    });
};

module.exports.seed = (knex, Promise) => {
    return dropTables(knex, Promise).then(() => {
        return createTables(knex, Promise).then(() => {
            return populateData(knex, Promise);
        });
    });
};
