'use strict'

/**
 * Catalogs controller
 *
 * Every functions are used for each API routes
 */

const Promise  = require('bluebird');
const Catalogs = require('../models/Catalogs.model');

// Returns every catalogs : /catalogs
module.exports.getAllCatalogs = async (req, res) => {
    let page = parseInt(req.query.page) || 1;
    page    -= 1;
    if (page < 0) {
        page = 0;
    }

    let limit = req.query.limit || 50;
    limit     = Math.min(parseInt(limit), 50);

    let offset = limit * page;

    let catalogs;
    try {
        catalogs = await Catalogs.query().eager('products').limit(limit).offset(offset);
    } catch (err) {
        console.log(err);

        return res.status(400).json('Error while getting catalogs');
    }

    return res.status(200).json(catalogs);
};

// Returns every products for a specific catalog : /catalogs/:catalog_id/products
module.exports.getCatalogProducts = (req, res) => {
    return res.status(200).json(req.catalog);
};
