'use strict'

/**
 * Products controller
 *
 * Every functions are used for each API routes
 */

const Promise  = require('bluebird');
const Products = require('../models/Products.model');

// Adds a product to a specific catalog : /catalogs/:catalog_id/products
module.exports.addProduct = async (req, res) => {
    let product        = req.body;
    product.catalog_id = req.catalog.id;

    let new_product;
    try {
        new_product = await Products.query().insertAndFetch(product);
    } catch (err) {
        console.log(err);

        return res.status(400).json('Error while adding product');
    }

    return res.status(201).json(new_product);
};

// Updates a product : /products/:product_id
module.exports.modifyProduct = async (req, res) => {
    let product = req.body;

    let updated_product;
    try {
        updated_product = await Products.query().patchAndFetchById(req.product.id, product);
    } catch (err) {
        console.log(err);

        return res.status(400).json('Error while updating product ' + req.product.id);
    }

    return res.status(200).json(updated_product);
}

// Removes a product : /products/:product_id
module.exports.deleteProduct = async (req, res) => {
    let deleted_product;
    try {
        Products.query().delete().where('id', req.product.id);
    } catch (err) {
        console.log(error);

        return res.status(400).json('Error while deleting product ' + req.product.id);
    }

    return res.status(200).json('Product ' + req.product.id + ' deleted');
}
