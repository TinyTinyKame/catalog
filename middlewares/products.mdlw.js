'use strict'

/**
 * All middlewares used for products
 */
const Promise  = require('bluebird');
const Products = require('../models/Products.model');

// Returns a product from its ID
module.exports.getProductById = async (req, res, next) => {
    let product;

    try {
        product = await Products.query().findById(req.params.product_id);
    } catch (err) {
        console.log(err);

        return res.status(400).json('Error while fetching product ' + req.params.product_id);
    }

    if (product === undefined) {
        let error    = new Error('Product ' + req.params.product_id  + ' not found');
        error.status = 404;
        next(error);
    }

    req.product = product;

    next();
}
