'use strict'

/**
 * All middlewares used for catalogs
 */
const Promise  = require('bluebird');
const Catalogs = require('../models/Catalogs.model');

// Returns a catalog from its ID
module.exports.getCatalogById = async (req, res, next) => {
    let catalog;

    try {
        catalog = await Catalogs.query().eager('products').findById(req.params.catalog_id);
    } catch (err) {
        console.log(err);

        return res.status(400).json('Error while fetching catalog ' + req.params.catalog_id);
    }

    if (catalog === undefined) {
        let error    = new Error('Catalog ' + req.params.catalog_id  + ' not found');
        error.status = 404;
        next(error);
    }

    req.catalog = catalog;

    next();
}
