'use strict'

/**
 * Every routes for products
 */
const router        = require('express').Router();
const ProductsCtrl  = require('../controllers/products.ctrl');
const ProductsValid = require('../validators/products.valid');
const ProductsMdlw  = require('../middlewares/products.mdlw');
const CatalogsValid = require('../validators/catalogs.valid');
const CatalogsMdlw  = require('../middlewares/catalogs.mdlw');

// Adds a product to a specific catalog
router.post(
    '/catalogs/:catalog_id/products',
    CatalogsValid.checkCatalogId,
    CatalogsMdlw.getCatalogById,
    ProductsValid.checkProductParams,
    ProductsCtrl.addProduct
);

// Updates a product
router.put(
    '/products/:product_id',
    ProductsValid.checkProductId,
    ProductsValid.checkProductParams,
    ProductsMdlw.getProductById,
    ProductsCtrl.modifyProduct
);

// Removes a product
router.delete(
    '/products/:product_id',
    ProductsValid.checkProductId,
    ProductsMdlw.getProductById,
    ProductsCtrl.deleteProduct
);

module.exports = router;
