'use strict'

/**
 * Every routes for catalogs
 */
const router        = require('express').Router();
const CatalogsCtrl  = require('../controllers/catalogs.ctrl');
const CatalogsValid = require('../validators/catalogs.valid');
const CatalogsMdlw  = require('../middlewares/catalogs.mdlw');

// Returns every catalogs
router.get('/catalogs', CatalogsCtrl.getAllCatalogs);

// Returns every products for a specific catalog
router.get(
    '/catalogs/:catalog_id/products',
    CatalogsValid.checkCatalogId,
    CatalogsMdlw.getCatalogById,
    CatalogsCtrl.getCatalogProducts
);

module.exports = router;
